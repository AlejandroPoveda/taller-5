class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.572489,-74.104084]
        this.escalaInicial=15;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };

        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion){

        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono(posicion, configuracion) {

        this.poligonos.push(L.polygon(posicion, configuracion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }

}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.572489,-74.104084]);
miMapa.colocarCirculo([4.572989,-74.107084], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.2,
    radius: 200
});



miMapa.colocarPoligono([
    [4.577989,-74.112084],
    [4.572989,-74.117084],
    [4.572989,-74.127084],
    [4.577989,-74.132084],
    [4.582989,-74.122084]
    ],{
        color: 'blue',
        weight:7
    });
